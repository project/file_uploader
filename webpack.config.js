const webpack = require("webpack");
const path = require("path");
const glob = require("glob");
const globSassImporter = require("node-sass-glob-importer");
const TerserPlugin = require("terser-webpack-plugin");
const autoprefixer = require("autoprefixer");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (env, argv) => {
  const directory = path.resolve(".");
  const workspace = path.parse(directory).base;

  // Get entry points from the package directory.
  const entries = {};
  glob.sync(`${directory}/components/*/*.js`).forEach((file) => {
    const { name } = path.parse(file);
    entries[name === "index" ? workspace : name] = file;
  });
  if (!Object.keys(entries).length) {
    process.exit(0);
  }

  return {
    mode: argv.mode ? argv.mode : "development",
    devtool: argv.mode ? false : "source-map",
    cache: argv.mode !== "production",
    stats: "errors-only",
    infrastructureLogging: {
      appendOnly: true,
      level: "log",
    },
    watchOptions: {
      poll: 1000,
    },
    entry: entries,
    module: {
      rules: [
        {
          test: /\.scss$/,
          include: path.resolve(directory, "components"),
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader",
            },
            {
              loader: "postcss-loader",
              options: {
                postcssOptions: {
                  plugins: () => [autoprefixer],
                },
              },
            },
            {
              loader: "sass-loader",
              options: {
                sassOptions: {
                  importer: globSassImporter(),
                },
              },
            },
          ],
        },
        {
          test: /\.js$/,
          include: path.resolve(directory, "components"),
          use: [
            {
              loader: "babel-loader",
              options: {
                presets: [["@babel/preset-env", { modules: "commonjs" }]],
                cacheDirectory: true,
              },
            },
          ],
        },
      ],
    },
    optimization: {
      removeAvailableModules: false,
      minimizer: [new TerserPlugin({ extractComments: false })],
    },
    output: {
      path: path.resolve(directory, "public"),
      filename: "js/[name].js",
      assetModuleFilename: "assets/[name][ext][query]",
    },
    plugins: [
      new webpack.DefinePlugin({
        importAll: (resolve) => {
          resolve.keys().forEach(resolve);
        },
      }),
      new MiniCssExtractPlugin({ filename: "css/[name].css" }),
    ],
    externals: {
      jquery: "jQuery",
    },
  };
};
